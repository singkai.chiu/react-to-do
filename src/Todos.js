import React from 'react'


const Todos = ({todos, deleteTodo}) =>{

    const todoList = todos.length ? ( todos.map(todo =>{

        //arrow function used in deleteTodo as this prevents automatic invokation
        return (
            <div className ="collection-item" key={todo.id}>

                <span onClick={() => {deleteTodo(todo.id)}}>{todo.content} </span>
            </div>
        )})) : (<p className= "center"> All to do's have been done </p>);

    return (
        <div className ="todos collection">
            {todoList}
        </div>
    )

}

export default Todos;